FROM node:16.13 as build

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package*.json /usr/src/app/
RUN npm install --only=production
COPY . /usr/src/app

FROM node:16.13
WORKDIR /usr/src/app
COPY --from=build /usr/src/app .
CMD ["node","server.js"]
