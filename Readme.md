
# Description

Simple script to subscribe to a PowUnity user's devices' event stream and print events to stdout.

# Usage

Obtain a token by following the instructions on the B2B Wiki Page or by contacting your B2B support contact.

## Using Docker

- Clone this repo
- `docker build -t tmp-powunity-integration .`
- `docker run -e POWUNITY_TOKEN="xxx" -e POWUNITY_SERVER_URL="https://traccar.powunity.com/api" tmp-powunity-integration`

## Using node (Alternative to Docker)

Set the environment variables `POWUNITY_TOKEN="xxx"`, `POWUNITY_SERVER_URL="https://traccar.powunity.com/api"`, then `npm start`.
