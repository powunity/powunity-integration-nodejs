const axios = require('axios');
const WebSocket = require('ws');

// === INTERFACE TO POWUNITY

/***
 * Creates a session from the given api using the supplied token.
 */
async function createSessionFromToken(api, token) {
	console.log(`Opening session`);
	try {
		const res = await api.get(`session`, { params: { token } });
		if (200 !== res.status) throw new Error(`Error from server, status=${res.status}`);
		if (!res.headers && !res.headers['set-cookie']) throw new Error(`Server did not respond with a session id`);
		const sessionId = res.headers['set-cookie'][0];
		if (!sessionId && !sessionId.startsWith('JSESSIONID=')) throw new Error(`Invalid session id received from server`);
		return sessionId;
	} catch (err) {
		if (err.response && 404 === err.response.status) throw new Error(`Could not create a session, are you sure your token is valid?`);
		throw(err);
	}
}

/***
 * Returns an instance of a WebSocket client connected the API
 * at serverUrl and authenticated with sessionId
 */
async function createWsClient(serverUrl, sessionId) {
	const wsUrl = serverUrl.replace(/^http/, 'ws') + '/socket';
	console.log(`Connecting to wsUrl=${wsUrl}`);
	return new WebSocket(wsUrl, { headers: { Cookie: sessionId } });
}

/***
 * Invalidates the given session on the given api
 */
async function invalidateSession(api, sessionId) {
	const res = await api.delete(`session`, { headers: { Cookie: sessionId } });
	console.log(`Session destroyed with status=${res.status} ok=${204 === res.status}`);
}

/***
 * Invalidates the current session and exits the current script with code 0
 */
function armSessionDestoryOnExit(api, sessionId) {
	const shutdown = () => {
		process.removeAllListeners('SIGINT');
		process.removeAllListeners('SIGTERM');
		console.log(`Shutdown signal received, invalidating session and then exiting...`);
		return invalidateSession(api, sessionId)
			.catch((err) => console.log(`Warn: Could not invalidate session before exiting`, err))
			.finally(() => process.exit(0));
	};
	process.on('SIGTERM', shutdown);
	process.on('SIGINT', shutdown);
	console.log(`Session will be attempted to be destroyed then this process exits`);
	return sessionId;
};

// === Set everything up. Ctrl-C to exit.

const TOKEN = process.env.POWUNITY_TOKEN;
const SERVER_URL = process.env.POWUNITY_SERVER_URL || 'https://traccar.powunity.com/api';

if (!TOKEN) throw new Error(`Missing POWUNITY_TOKEN in env`);
if (!SERVER_URL) throw new Error(`Missing POWUNITY_SERVER_URL in env`);

const API = axios.create({
	baseURL: SERVER_URL,
	timeout: 15000
});

createSessionFromToken(API, TOKEN)
	.then(sessionId => armSessionDestoryOnExit(API, sessionId)) // this line is optional but a good/secure idea
	.then(sessionId => createWsClient(API.defaults.baseURL, sessionId))
	.then(socket => {

		socket.onclose = (event) => console.log('socket closed');
		socket.onerror = (err) => console.log('Failed connecting to websocket', err);

		socket.onmessage = (event) => console.log(`MESSAGE`, JSON.stringify(JSON.parse(event.data), null, 2));
		console.log(`Waiting for messages from server. They will be printed here.`);
	})
	.catch((err) => {
		console.log(`ERROR, shutting down due to`, err);
		process.exit(9);
	});
